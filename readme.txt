IIC2333 SISTEMAS OPERATIVOS Y REDES: Tarea 3

Tomás Günther -- Fernando Martínez

Cliente: 
	Compilar: usamos en la carpeta Cliente "gcc main_cliente -o client"
	Ejecutar: usamos "./client <ipAdress". Para probar simplemente usamos "./client localhost"

Servidor
	Compilar: usamos en la carpeta Servidor "gcc main_server.c -o server -lssl -lcrypt"
	Ejecutar: usamos "./server".
