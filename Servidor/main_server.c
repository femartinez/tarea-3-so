
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>


// socket stuff
#include <sys/socket.h>
#include <sys/types.h>

// IP Address conversion
#include <arpa/inet.h>

// INET constants
#include <netinet/in.h>

//SSL sockets
#include <openssl/rand.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>



#define PORT 20500
#define BUF_SIZE 2048

static char* word;
static int engines;

// Simple structure to keep track of the handle, and
// of what needs to be freed later.
typedef struct {
  int socket;
  SSL *sslHandle;
  SSL_CTX *sslContext;
} connection;

// For this example, we'll be testing on openssl.org
#define SERVER_GOOGLE  "www.google.com"
#define SERVER_YAHOO "search.yahoo.com"
#define PORT_SSL 443

char** str_split(char* a_str, const char a_delim)
{
  char** result    = 0;
  size_t count     = 0;
  char* tmp        = a_str;
  char* last_comma = 0;
  char delim[2];
  delim[0] = a_delim;
  delim[1] = 0;

    /* Count how many elements will be extracted. */
  while (*tmp)
  {
    if (a_delim == *tmp)
    {
      count++;
      last_comma = tmp;
    }
    tmp++;
  }

    /* Add space for trailing token. */
  count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
  count++;
  count++;

  result = malloc(sizeof(char*) * count);

  if (result)
  {
    size_t idx  = 0;
    char* token = strtok(a_str, delim);

    while (token)
    {
      assert(idx < count);
      *(result + idx++) = strdup(token);
      token = strtok(0, delim);
    }
    assert(idx < count - 1);
    *(result + idx++) = strdup("");
    assert(idx = count - 1);
    *(result + idx) = 0;
  }

  return result;
}


//Metodos de string
char *substring(char *string, int position, int length) 
{
 char *pointer;
 int c;
 
 pointer = malloc(length+1);
 
 if (pointer == NULL)
 {
  printf("Unable to allocate memory.\n");
  exit(EXIT_FAILURE);
}

for (c = 0 ; c < position -1 ; c++) 
  string++; 

for (c = 0 ; c < length ; c++)
{
  *(pointer+c) = *string;      
  string++;   
}

*(pointer+c) = '\0';

return pointer;
} 

char** split_by_word(char* str, char* delim, int* i){
  //char** retorno[1024*4][1024*4];

  char** retorno = malloc(sizeof(char)*sizeof(char)*1024);
  (*i)= 0;
  bool isStringLeft = true;
  while(isStringLeft){

    int uno = (int)(strstr(str, delim) - str);
    if (uno > 0){
      char* a = substring(str, 0, uno);
      
      str = substring(str, uno + strlen(delim) + 1, strlen(str) - (uno + strlen(delim) ));
      
      // printf("%s\n", a);
      // printf("Resto:\n");
      // printf("%s\n", str);  

      // printf("-----------------\n");
      // printf("%i\n", (*i));
      retorno[(*i)]= a;
      (*i)++;
    }
    else{
      retorno[(*i)]= str;
      (*i)++;
      isStringLeft = false;
    }  
  }
  return retorno;
}

// Establish a regular tcp connection
int tcpConnect (char * SERVER)
{
  int error, handle;
  struct hostent *host;
  struct sockaddr_in server;

  host = gethostbyname (SERVER);
  handle = socket (AF_INET, SOCK_STREAM, 0);
  if (handle == -1)
  {
    perror ("Socket");
    handle = 0;
  }
  else
  {
    server.sin_family = AF_INET;
    server.sin_port = htons (PORT_SSL);
    server.sin_addr = *((struct in_addr *) host->h_addr);
    bzero (&(server.sin_zero), 8);

    error = connect (handle, (struct sockaddr *) &server,
     sizeof (struct sockaddr));
    if (error == -1)
    {
      perror ("Connect");
      handle = 0;
    }
  }

  return handle;
}

// Establish a connection using an SSL layer
connection *sslConnect (char * SERVER)
{
  connection *c;

  c = malloc (sizeof (connection));
  c->sslHandle = NULL;
  c->sslContext = NULL;

  c->socket = tcpConnect (SERVER);
  if (c->socket)
  {
      // Register the error strings for libcrypto & libssl
    SSL_load_error_strings ();
      // Register the available ciphers and digests
    SSL_library_init ();

      // New context saying we are a client, and using SSL 2 or 3
    c->sslContext = SSL_CTX_new (SSLv23_client_method ());
      // if (c->sslContext == NULL)
      //   ERR_print_errors_fp (stderr);

      // Create an SSL struct for the connection
    c->sslHandle = SSL_new (c->sslContext);
    // if (c->sslHandle == NULL)
    //    ERR_print_errors_fp (stderr);

      // Connect the SSL struct to our connection
    SSL_set_fd (c->sslHandle, c->socket);
      // if (!SSL_set_fd (c->sslHandle, c->socket))
      //   ERR_print_errors_fp (stderr);

      // Initiate SSL handshake
    SSL_connect (c->sslHandle);
      // if (SSL_connect (c->sslHandle) != 1)
      //   ERR_print_errors_fp (stderr);
  }
  else
  {
    perror ("Connect failed");
  }

  return c;
}

// Disconnect & free connection struct
void sslDisconnect (connection *c)
{
  if (c->socket)
    close (c->socket);
  if (c->sslHandle)
  {
    SSL_shutdown (c->sslHandle);
    SSL_free (c->sslHandle);
  }
  if (c->sslContext)
    SSL_CTX_free (c->sslContext);

  free (c);
}

// Read all available text from the connection
char *sslRead (connection *c)
{
  const int readSize = 1024;
  char *rc = NULL;
  int received, count = 0;
  char buffer[1024];

  if (c)
  {
    while (1)
    {
      if (!rc)
        rc = malloc (readSize * sizeof (char) + 1);
      else
        rc = realloc (rc, (count + 1) *
          readSize * sizeof (char) + 1);

      received = SSL_read (c->sslHandle, buffer, readSize);
      buffer[received] = '\0';
          //printf("%i\n",received);
      if (received > 0){
        strcat (rc, buffer);
            //printf("!!!! %s\r\n", buffer);
      }

      if (received <= 0)
        break;
      count++;
    }
  }

  return rc;
}

// Write text to the connection
void sslWrite (connection *c, char *text)
{
  if (c)
    SSL_write (c->sslHandle, text, strlen (text));
}

char* formulate_query(char* word){
  char* query = malloc(sizeof(char)*1024*4);
  char* primera_parte = "GET /search?q=";
  strcpy(query,primera_parte);  
  strcat(query, word);
  char *segunda_parte = " HTTP/1.0\r\n\r\n" ;
  strcat(query, segunda_parte);
  return query;
}

char* google_search(char* word, int amount_results) {
  int counter = 1;
  char * response;
  char * formatted_response;
  connection *c;
  c = sslConnect (SERVER_GOOGLE);
  char* query = formulate_query(word);
  //Hacer request
  //printf("%s\n", query);
  sslWrite (c, query);
  //Obtener respuesta
  response = sslRead (c);
  //Procesar respuesta
  //printf("%s\n", response);

  int i = 0;
  int it = 0;
  char** li = split_by_word(response, "<li class=\"g\">", &i);
  it = 1;
  strcpy(formatted_response, "\"Google\":{");
  
  while(it < i && it < amount_results + 1)
  {
    strcat(formatted_response, "\"" );
    char base[] = "";
    char filename[5];
    sprintf(filename, "%s%d", base, (it));
    strcat(formatted_response, filename );
    strcat(formatted_response, "\":{\"text\": \"");
    printf("%s\n\n\n", li[it]);

    int i2 = 0;
    char** href = split_by_word(li[it], "href", &i2);

    //printf("%s\n", href[0]);
    //printf("%s\n", href[1]);
    //printf("%s\n", href[1]);

    i2 = 0;
    char** link = split_by_word(href[1], "\"", &i2);



    //printf("\n");printf("\n");printf("\n");
    //printf("%s\n", link[1]);

    //printf("\n");printf("\n");printf("\n");
    // printf("%s\n",li[it]);
    char** text = split_by_word(link[2], "</a>", &i2);

    //printf("<\n");

    //printf("%s\n", substring(text[0], 1, strlen(text[0]) - 1));
    //printf("%s\n", substring(text[1], 0, strlen(text[1]) - 3));
    

    strcat(formatted_response, substring(text[0], 2, strlen(text[0]) -2));
    strcat(formatted_response, "\", \"url\": \"");
    int aa = 1;
    while(aa < 3){
      if (strstr(link[aa],"http")){
        strcat(formatted_response, substring(link[aa], 8, strlen(link[aa]) - 8));
        strcat(formatted_response, "\"},");
        break; 
      }
      else if(aa == 3){
        strcat(formatted_response, substring(link[1], 8, strlen(link[1]) - 8));
        strcat(formatted_response, "\"},");
      }
      else{
        aa++;
      }
    }
    
    // printf("\n");
    it++;
  }
  strcpy(formatted_response, substring(formatted_response, 0, strlen(formatted_response) - 1));
  strcat(formatted_response, "}");
  return formatted_response;

}

char* yahoo_search(char* word, int amount_results) {
  int counter = 1;
  char * response;
  char * formatted_response = malloc(sizeof(char)*1024);
  connection *c;
  c = sslConnect (SERVER_YAHOO);
  char* query = formulate_query(word);
  //Hacer request
  //printf("%s\n", query);
  sslWrite (c, query);
  //Obtener respuesta
  response = sslRead (c);
  //Procesar respuesta
  //printf("%s\n", response);

  int i = 0;
  int it = 0;
  char** li = split_by_word(response, "<li><div class=\"res\">", &i);
  it = 1;
  strcpy(formatted_response, "\"Yahoo\":{");
  free(li[0]);
  free(response);
  //printf("%s\n\n\n", li[0]);
  // printf("%s\n\n\n", li[1]);
  // printf("%s\n\n\n", li[2]);
  // printf("%s\n\n\n", li[3]);
  // printf("%s\n\n\n", li[4]);

  while(it < i && it < amount_results + 1)
  {
    //printf("%s\n", li[0]);

    strcat(formatted_response, "\"" );
    char base[] = "";
    char filename[5];
    sprintf(filename, "%s%d", base, (it));
    strcat(formatted_response, filename );
    strcat(formatted_response, "\":{\"text\": \"");


    int i2 = 0;
    
    char** href = split_by_word(li[it], "href", &i2);
    //char** link = malloc(sizeof(char)*sizeof(char)*1024);
    //printf("\n\n\n\n%s\n", href[0]);
    printf("%s\n\n\n\n", href[1]);
    //printf("\n\n\n\n");
    
    i2 = 0;
    char** link = split_by_word(href[1], "\">", &i2);



    //printf("\n");printf("\n");printf("\n");
    //printf("%s\n", link[0]);
    //printf("%s\n", link[1]);
    //printf("%s\n", link[2]);

    //printf("\n");printf("\n");printf("\n");
    // printf("%s\n",li[it]);
    char** text = split_by_word(link[1], "</a>", &i2);

    //printf("<\n");

    //printf("%s\n", substring(text[0], 1, strlen(text[0]) - 1));
    //printf("%s\n", substring(text[1], 0, strlen(text[1]) - 3));
    

    strcat(formatted_response, substring(text[0], 1, strlen(text[0])));
    //printf("%s\n", substring(text[1], 0, strlen(text[1]) - 3));
    //printf("%s\n", substring(text[2], 0, strlen(text[2]) - 3));

    //strcat(formatted_response, "<");
    //strcat(formatted_response, substring(text[1], 0, strlen(text[1]) - 3));
    strcat(formatted_response, "\", \"url\": \"");
    i2 = 0;
    char** url = split_by_word(link[0], "\"", &i2);
    strcat(formatted_response, substring(url[1], 0, strlen(url[1])));
    strcat(formatted_response, "\"},");
    
    // printf("\n");
    it++;
  }
  strcpy(formatted_response, substring(formatted_response, 0, strlen(formatted_response) - 1));
  strcat(formatted_response, "}");
  return formatted_response;






  // int counter = 1;
  // char * formatted_response;
  // char * response;
  // connection *c;
  // c = sslConnect (SERVER_YAHOO);
  // char* query = formulate_query(word);
  // //Hacer request
  // sslWrite (c, query);
  // //Obtener respuesta
  // response = sslRead (c);
  // //Procesar respuesta

  // return formatted_response;
}


char* search_internet(int engines, char* word, int amount_results){
  char *response = malloc(sizeof(char)*1024*16);
  strcpy(response, "{\"result\":{");
  char *google_res = malloc(sizeof(char)*1024*16);
  char *yahoo_res = malloc(sizeof(char)*1024*16);

  //engine 1: google, 2: yahoo
  printf(" \n");
  if (engines == 1 || engines == 3)
  {
    strcat(google_res,google_search(word, amount_results));
    strcat(response, google_res);
  }
  if(engines == 3)
  {
    strcat(response, ",");
  }
  if(engines == 2 || engines == 3)
  {
    strcat(yahoo_res,yahoo_search(word, amount_results));
    strcat(response, yahoo_res);
  }
  //Cerramos los resultados
  strcat(response, "}}");
  return response;
}


char** split(char* text, const char a_delim){
  char blank[100];
  int c = 0, d = 0;
  while (text[c] != '\0')
  {
    if (!(text[c] == ' ' && text[c+1] == ' ')) 
    {
      blank[d] = text[c];
      d++;
    }
    c++;
  }
  blank[d] = '\0';
  //printf("%s\n", blank); 
  //char* texts;
  //strcpy(texts, blank);
  return str_split(blank, ' ');
}

int isNumeric (const char * s)
{
    if (s == NULL || *s == '\0' || isspace(*s))
      return 0;
    char * p;
    strtod (s, &p);
    return *p == '\0';
}

bool loggedUser(int clientSocket){
  int action = 0;
  char buffer[BUF_SIZE];
  int bytesReceived;
  bool isConnected = true;
  while(isConnected)
  {
    // receive any data from the client 
    bytesReceived = recv( clientSocket, buffer, BUF_SIZE, 0 );
    // terminate the bytes as a string and print the result
    buffer[bytesReceived]= '\0';
    //printf("C2: %s\n", buffer);

    char replyText[BUF_SIZE];
    strcpy(replyText, "");
    char** tokens;
    tokens = split(buffer, ' ');

    int i;
    // for (i = 0; *(tokens + i); i++)
    // {
    //   printf("month=[%s]\n", *(tokens + i));
    // }           


    if (tokens)
    {
      if (!strcmp(*(tokens), "SEARCH") && action == 0)
      {
        action = 1;
        strcat(replyText, "OK, WORD?");
      }
      else if (strcmp(*(tokens), "") && !strcmp(*(tokens + 1), "") && action == 1)
      {
        action = 2;
        char* temp = *(tokens);
        strcpy(word, temp);
        strcat(replyText, "OK, ENGINES?");
      }
      else if (strcmp(*(tokens), "") && action == 2)
      {
        action = 3;
        strcat(replyText, "OK, RESULTS?");  
        if (!strcmp(*(tokens), "Google")){
          engines = 1;
        }
        else if (!strcmp(*(tokens), "Yahoo")){
          engines = 2; 
        }
        else if (!strcmp(*(tokens), "Google,Yahoo")){
          engines = 3;
        }
        else if (!strcmp(*(tokens), "Yahoo,Google")){
          engines = 3;
        }
        else{
          action = 2;
          strcpy(replyText, "");
          strcat(replyText, "NOT OK, ENGINE?");  
        }
      }
      else if (strcmp(*(tokens), "") && !strcmp(*(tokens + 1), "") && action == 3)
      {

        
        //resultados = cantidad de resultados
        //word = palabra a buscar (char[])
        //engines = 1 = google, 2 yahoo, 3 las 2
        if (isNumeric(*(tokens))){
          int resultados = atoi(*(tokens));
          if (resultados <= 0 || resultados > 5 )
            strcpy(replyText, "NOT OK, RESULTS?");
          else{                  
            action = 0;
            //Llamamos al método que nos entrega la búsqueda en internet y lo concatenamos al string reply text
            strcpy(replyText, search_internet(engines, word,resultados));
          }
        }
        else{
          strcpy(replyText, "NOT OK, RESULTS?");
        }
      }
      else
      {
        action = 0;
        strcat(replyText, "NOT OK, COMMAND?");                
      }

      
    }
    else
    {
      strcat(replyText, "NOT OK, COMMAND?");   
    }
    if (!strcmp(*(tokens), "QUIT"))
    {
      strcpy(replyText, "");
      strcat(replyText, "BYE!");
      isConnected = false;
    }

    int ii;
    for (ii = 0; *(tokens + ii); ii++)
    {
      free(*(tokens + ii));
    }              
    free(tokens);

    strncpy( buffer, replyText, strlen( replyText ) );
    buffer[strlen(replyText)] = '\0';
    printf("S: %s\n", buffer);

    send( clientSocket, buffer, strlen( buffer ), 0 );

  }
  return false;
}






int main( int argc, char *argv[] ) {
   // descriptor for the socket we'll use as the server
 int serverSocket;

 word = malloc(sizeof(char)*1024*16);
   // descriptor for an accepted connection to a client
 int clientSocket;

   // socket internet address data for the server
 struct sockaddr_in serverData;

   // socket internet address data for a client
 struct sockaddr_in clientData;
 int clientDataLength;

   // buffer for incoming/outgoing data
 char buffer[BUF_SIZE];

 bool isConnected;

 int bytesReceived;

   /* 
    * Set up the server 
    */
    
    bzero( &serverData, sizeof( serverData ) );
   // use the Internet Address Family (IPv4)
    serverData.sin_family = AF_INET;
   // accept connections from a client on any address
    serverData.sin_addr.s_addr = htonl( INADDR_ANY );
   // set the port for incoming packets
    serverData.sin_port = htons( PORT );

   /* 
    * Open a TCP/IP (stream) socket 
    * (and save the descriptor so we can refer to it in the future)
    */
   // we're using the internet protocol family and the TCP/IP protocol
    serverSocket = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP );

   // bind the socket to the address
    int bindResult = bind( serverSocket, 
      (struct sockaddr *)&serverData,
      sizeof( serverData ) );
    if ( bindResult < 0 ) {
      printf("Error: Unable to bind socket");
      close(serverSocket);
    }  else if ( listen( serverSocket, 10 ) < 0 ) {
      // listening for incoming connections failed
      printf( "Error: Unable to listen" );
      close( serverSocket );
    }else {

      while (true){
        // it's important to specify this size first
        clientDataLength = sizeof(clientData);
        
        // accept the incoming connection request
        clientSocket = accept(serverSocket, 
         (struct sockaddr *)&clientData,
         &clientDataLength);
        assert(clientSocket > 0); // ensure we have a valid socket
        
        char *clientAddress = inet_ntoa( clientData.sin_addr );
        int clientPort = ntohs( clientData.sin_port );
        
        printf( "Accepted Connection from: %s:%d\n", clientAddress, clientPort );
        
        isConnected = true;
        while ( isConnected ) {
          // receive any data from the client 
          bytesReceived = recv( clientSocket, buffer, BUF_SIZE, 0 );

          // terminate the bytes as a string and print the result
          buffer[bytesReceived]= '\0';
          printf("C: %s\n", buffer);

          char replyText[BUF_SIZE];
          strcpy(replyText, "");
          char** tokens;
          tokens = split(buffer, ' ');

          // int i;
          // for (i = 0; *(tokens + i); i++)
          // {
          //     printf("month=[%s]\n", *(tokens + i));
          //     //free(*(tokens + i));
          // }              
          //free(tokens);
          int next_event = 0;

          if (tokens)
          {
            if (!strcmp(*(tokens), "HI,") && !strcmp(*(tokens + 1), "I") && !strcmp(*(tokens + 2), "AM")){
              if (strcmp(*(tokens + 3), "")){
                strcat(replyText, "HI ");
                strcat(replyText, *(tokens + 3));
                next_event = 1;
              }
              else{
                strcat(replyText, "NOT OK, WHO ARE YOU?");                  
              }
            }
            else{
              strcat(replyText, "NOT OK, WHO ARE YOU?");                
            }


          }
          else{
            strcat(replyText, "NOT OK, WHO ARE YOU?");   
          }

          if (!strcmp(*(tokens), "QUIT")){
            strcpy(replyText, "");
            strcat(replyText, "BYE!");
            isConnected = false;
          }
          int i = 0;
          for (i = 0; *(tokens + i); i++)
          {
            free(*(tokens + i));
          }              
          free(tokens);

          strncpy( buffer, replyText, strlen( replyText ) );
          printf("S: %s\n", replyText);

          send( clientSocket, buffer, strlen( replyText ), 0 );


          if (next_event == 1)
          {
            isConnected = loggedUser(clientSocket);
          }
        }
        
        close( clientSocket );

      }
    }


    close( serverSocket );

    return 0;
  }