#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

#define PORT 20500
#define BUF_SIZE 1024*20

int main( int argc, char *argv[] ) 
{
  if ( argc != 2 )
  {
    printf( "Uso: %s <IP Address>\n", argv[0] );
    exit( 0 );
  }

   // get the host address from the first commandline argument
  char *address = argv[1];

   // descriptor number for the socket we'll use
  int serverSocket;

   // socket internet address data for the connection to the server
  struct sockaddr_in serverData;

   // host data
  struct hostent *host;

   // buffer for sending and receiving data
  char buffer[BUF_SIZE];
  char BUFFER_RECPTIOON[BUF_SIZE];
  bool isConnected;

  int bytesReceived;

   /* 
    * Set up the connection to a server 
    */
    
    bzero( &serverData, sizeof( serverData ) );
   // use the Internet Address Family (IPv4)
    serverData.sin_family = AF_INET;
   // get host data from the host address
    host = gethostbyname( address );
   // copy the address data from the host struct over to the server address struct
    bcopy( host->h_addr, &(serverData.sin_addr.s_addr), host->h_length);
   // set the port to connect to
    serverData.sin_port = htons( PORT );

   /* 
    * Open a TCP/IP socket to the server
    * (and save the descriptor so we can refer to it in the future)
    */
    serverSocket = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP );

   // connect to the server
    if ( connect( serverSocket,(struct sockaddr *)&serverData, sizeof( serverData ) ) < 0 ) 
    {
      printf( "Error connecting\n" );
      close( serverSocket );
    } else 
    {

      isConnected = true;
      while ( isConnected ) 
      {
         // send some data
       printf( "C: " );

       fgets( buffer, BUF_SIZE, stdin );

       char* temp = strtok(buffer, "\n");
       strcpy(buffer, temp);
       buffer[strlen(temp)]='\0';
       //printf("%s\n", buffer);

       send( serverSocket, buffer, strlen( buffer ), 0 );

         // wait to receive data from the server
       bytesReceived = recv( serverSocket, buffer, BUF_SIZE, 0 );

         // terminate the bytes as a string and print the result
       buffer[bytesReceived]= '\0';
       printf("S:");
       char *token;
       char *subtoken;
       char *subsubtoken;

       char *saveptr1;
       char *saveptr2;
       char *saveptr3;
       char *output = malloc(sizeof(char)*1024*50);
       char *texto;
       if(strstr(buffer, "result"))
       {
        printf("%s\n", buffer);

        token = strtok_r(buffer,"{",&saveptr1);
        while(token != NULL){
          //printf("!!! %s\n",token);
          if(strstr(token,"\"Google\":")){
            strcat(output,"Google\n--------------\n");
          }

          if(strstr(token,"\"Yahoo\":" )&& !strstr(token,"text") ){
            strcat(output,"Yahoo\n--------------\n");
          }

          if(strstr(token,"\"1\":"))
          {
            strcat(output, "1) ");
          }
          else if(strstr(token,"text"))
          {
            bool yahoo = false;
            if(strstr(token,"\"Yahoo\":")){
              yahoo = true;
            }

            if(strstr(token,"}}"))
            {
              //Cuando es el último resultado de un buscador
              for (texto = token; ; texto = NULL) {
                subtoken = strtok_r(texto, ":", &saveptr2);
                if (subtoken == NULL)
                  break;
                //printf(" --> %s\n", subtoken);

                if(strstr(subtoken,"http")){
                  //este es para la primera parte del link
                  subsubtoken = strtok_r(subtoken,"\"",&saveptr3 );
                  while(subsubtoken != NULL){
                    strcat(output,subsubtoken);
                    subsubtoken = strtok_r(NULL, "\"",&saveptr3);
                  }
                  strcat(output,":");
                }
                else if(strstr(subtoken,", \"url\""))
                {
                  //Esto es para rescatar el titulo
                  subsubtoken = strtok_r(subtoken,"\"",&saveptr3 );
                  subsubtoken = strtok_r(NULL,"\",",&saveptr3 );
                  strcat(output,subsubtoken);
                  strcat(output,"\n");


                }
                else if(strstr(subtoken, "\"") && strstr(subtoken,"//"))
                {
                  //Esto es para rescatar la segunda parte del link
                  strcat(output, strtok_r(subtoken,"\"",&saveptr3));
                  strcat(output,"\n");
                }
                
              }
            }
            else
            {
              //Cuando no es el último resultado de un buscador
              //Cuando es el último resultado de un buscador
              for (texto = token; ; texto = NULL) {
                subtoken = strtok_r(texto, ":", &saveptr2);
                if (subtoken == NULL)
                  break;
                
                if(strstr(subtoken,"http")){
                  //esto es para rescatar la primera parte del link
                  subsubtoken = strtok_r(subtoken,"\"",&saveptr3 );
                  while(subsubtoken != NULL){
                    strcat(output,subsubtoken);
                    subsubtoken = strtok_r(NULL, "\"",&saveptr3);
                  }
                  strcat(output,":");
                }
                else if(strstr(subtoken,", \"url\""))
                {
                  //Esto es para rescatar el titulo
                  subsubtoken = strtok_r(subtoken,"\"",&saveptr3 );
                  subsubtoken = strtok_r(NULL,"\",",&saveptr3 );
                  strcat(output,subsubtoken);
                  strcat(output, "\n");
                }
                else if(strstr(subtoken, "\"") && strstr(subtoken,"//")){
                  //esto es para rescatar la segunda parte del link
                  strcat(output, strtok_r(subtoken,"\"",&saveptr3));
                  strcat(output,"\n\n");
                  subtoken = strtok_r(NULL,"\"",&saveptr3);
                  strcat(output, strtok_r(NULL,"\"",&saveptr3));
                  strcat(output,") ");
                }
              }

            }


            if(yahoo){
              strcat(output,"Yahoo\n--------------\n");
            }
          }




          token = strtok_r(NULL,"{",&saveptr1);

        }
        
        printf("%s\n", output);
      }
      else
      {
        printf( " %s\n", buffer );
      }

      if (!strcmp(buffer, "BYE!")){
        isConnected = false;
      }
      fflush(stdout);         // make sure everything makes it to the output
      buffer[0]='\0'; 
    }

    close( serverSocket );
  }

  return 0;
}